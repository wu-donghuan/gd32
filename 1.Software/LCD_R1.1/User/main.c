/********************************************************************************
  * 文 件 名: main.c
  * 版 本 号: v1.0
  * 修改作者: wudonghuan
  * 修改日期: 2023年04月15日
  * 功能介绍:          
  ******************************************************************************
  * 注意事项:
*********************************************************************************/
#include "sys.h"
#include "gd32f4xx.h"
#include "systick.h"
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "bsp_led.h"
#include "bsp_key.h"
#include "bsp_usart.h"
#include "bsp_dma.h"
#include "bsp_basic_timer.h"

#include "pic_480480.h"

#include "exmc_sdram.h"
#include "lcd.h"
#include "touch.h"

uint16_t POINT[5]={WHITE,BLUE,RED,YELLOW,GREEN};
uint16_t BACK[5]={LIGHTGREEN,LIGHTGRAY,LGRAY,LGRAYBLUE,GRAYBLUE};

/************************************************
函数名称 ： main
功    能 ： 主函数
参    数 ： 无
返 回 值 ： 无
作    者 ： wudonghuan
*************************************************/

uint16_t TIME = 0;

int main(void)
{

    systick_config();   // 滴答定时器初始化
//    led_gpio_config();  // led初始化
//    key_gpio_config();            // key初始化
//    usart_gpio_config(115200U);  // 串口0初始化
    usart0_init();
    delay_1ms(100);
//    printf("Hello!\r\n");               // 串口打印key release!
    basic_timer_config(20000,10000);  // 定时器初始化
//    dma_config();						// DMA配置

    //RGB屏需要Gram
    exmc_synchronous_dynamic_ram_init(EXMC_SDRAM_DEVICE0);
    
    /* RGB屏 */
    LCD_Init(); //显示屏初始化代码
    
    POINT_COLOR=POINT[4];
    BACK_COLOR=BACK[4];
    
//    LCD_Clear(1,LGRAYBLUE);
//    LCD_Clear(0,LGRAY);
    
//    LCD_ShowString(1,30,50,480,80,24,1,"waiting ");
//    LCD_ShowString(1,30,80,480,110,24,1,"waiting");
//    LCD_ShowString(1,30,110,480,110,24,1,"waiting");
//    LCD_ShowString(1,30,140,480,80,24,1,"waiting");

    delay_1ms(1000);
    delay_1ms(1000);
    //触摸屏
    
    GT1151_Init();
        TLI_Fill(0,0,0,479,799,BLUE);
        TLI_Fill(1,0,0,479,799,BLACK);//修改1
        
        tli_layer_disable(LAYER1);
        tli_layer_enable(LAYER0);
        tli_reload_config(TLI_FRAME_BLANK_RELOAD_EN);
        
    while(1) 
    {
//        delay_1ms(1000);
        while(ltdc_finish_state==1);
        ltdc_finish_state=1;
        
            /* 配置图层帧缓冲区基地址 */
          TLI_LxFBADDR(LAYER0) &= ~(TLI_LxFBADDR_FBADD);
          TLI_LxFBADDR(LAYER0) = (uint32_t)ltdc_framebuf[1];
//        text();   
//        tli_layer_disable(LAYER1);
//        tli_layer_enable(LAYER0);
        tli_reload_config(TLI_FRAME_BLANK_RELOAD_EN);
        
//        delay_1ms(1000);
        while(ltdc_finish_state==1);
        ltdc_finish_state=1;

//        tli_layer_disable(LAYER0);
//        tli_layer_enable(LAYER1);
//        text2();  
        
            TLI_LxFBADDR(LAYER0) &= ~(TLI_LxFBADDR_FBADD);
            TLI_LxFBADDR(LAYER0) = (uint32_t)ltdc_framebuf[0];
        tli_reload_config(TLI_FRAME_BLANK_RELOAD_EN);
//        tli_enable();
        //照片前进
//        for(int i=0;i<300;i=i+10)
//        {
//            while(ltdc_finish_state==0);
//            TIME++;
//          if(((i/10)%2)==1)
//          {
//            //要等待上一帧结束才能刷下一帧
//            TLI_Fill(0,0,0,479,799,BLUE);//修改0
//            TLI_Color_Fill(0,0,i,479,479+i,(uint16_t*)&gImage_image);//修改0
////            while(ltdc_finish_state==0);
//            tli_layer_disable(LAYER1);
//            tli_layer_enable(LAYER0);
//            TLI_LxFBADDR(0)=(uint32_t)ltdc_framebuf[0];
//            tli_reload_config(TLI_FRAME_BLANK_RELOAD_EN);
//            ltdc_finish_state = 0;
//          }
//          else if(((i/10)%2)==0)
//          {
//            TLI_Fill(1,0,0,479,799,BLACK);//修改1
//            TLI_Color_Fill(1,0,i,479,479+i,(uint16_t*)&gImage_image);////修改1
////            tli_layer_disable(LAYER0);
////            tli_layer_enable(LAYER1);
//            TLI_LxFBADDR(0)=(uint32_t)ltdc_framebuf[1];
//            tli_reload_config(TLI_FRAME_BLANK_RELOAD_EN); 
//            ltdc_finish_state = 0;              
//          }
//        }
//         //照片后退
//        for(int i=300;i>0;i=i-10)
//        {
//            while(ltdc_finish_state==0);
////            delay_1ms(10);
//            TIME++;

//          if(((i/10)%2)==1)
//          {
//            TLI_Fill(0,0,0,479,799,BLACK);
//            TLI_Color_Fill(0,0,i,479,479+i,(uint16_t*)&gImage_image);
//            tli_layer_disable(LAYER1);
//            tli_layer_enable(LAYER0);
//            tli_reload_config(TLI_FRAME_BLANK_RELOAD_EN);  
//            ltdc_finish_state = 0;
//          }
//          else
//          {
//            TLI_Fill(1,0,0,479,799,BLACK);
//            TLI_Color_Fill(1,0,i,479,479+i,(uint16_t*)&gImage_image);
//            tli_layer_disable(LAYER0);
//            tli_layer_enable(LAYER1);
//            tli_reload_config(TLI_FRAME_BLANK_RELOAD_EN);  
//            ltdc_finish_state = 0;
//          }
//        }     
        

    }
}
