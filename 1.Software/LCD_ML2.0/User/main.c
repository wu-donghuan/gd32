/********************************************************************************
  * 文 件 名: main.c
  * 版 本 号: v1.0
  * 修改作者: wudonghuan
  * 修改日期: 2023年04月15日
  * 功能介绍:          
  ******************************************************************************
  * 注意事项:
*********************************************************************************/
#include "sys.h"
#include "gd32f4xx.h"
#include "systick.h"
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "bsp_led.h"
#include "bsp_key.h"
#include "bsp_usart.h"
#include "bsp_dma.h"
#include "bsp_basic_timer.h"
#include "lcd.h"
#include "touch.h"

//LVGL需要包含的头文件
#include "lvgl.h"
#include "lv_port_disp.h"
#include "lv_port_indev.h"

//demo头文件
#include "lv_demo_music.h"

/************************************************
函数名称 ： main
功    能 ： 主函数
参    数 ： 无
返 回 值 ： 无
作    者 ： wudonghuan
*************************************************/

uint16_t POINT[5]={WHITE,BLUE,RED,YELLOW,GREEN};
uint16_t BACK[5]={LIGHTGREEN,LIGHTGRAY,LGRAY,LGRAYBLUE,LBBLUE};

void lv_ex_label(void)
{
    
    lv_obj_t* switch_obj = lv_switch_create(lv_scr_act());
    lv_obj_set_size(switch_obj, 120, 60);
    lv_obj_align(switch_obj, LV_ALIGN_CENTER, 0, 0);//对齐
    
    lv_obj_t* switch1_obj = lv_switch_create(lv_scr_act());
    lv_obj_set_size(switch1_obj, 120, 60);
    lv_obj_align(switch1_obj, LV_ALIGN_BOTTOM_MID, 0, 0);//对齐    
    
    lv_obj_t *spinner = lv_spinner_create(lv_scr_act(),1,1000);
    
    lv_obj_set_style_arc_color( spinner, lv_color_hex(0x4a9f00), LV_PART_MAIN ); 	   /* 设置主体圆弧颜色 */
    lv_obj_set_style_arc_color( spinner, lv_color_hex(0x83bd55), LV_PART_INDICATOR );   /* 设置指示器圆弧颜色 */
    
    lv_obj_set_style_arc_width( spinner, 25, LV_PART_MAIN );					    /* 设置主体圆弧宽度 */
    lv_obj_set_style_arc_width( spinner, 30, LV_PART_INDICATOR );				    /* 设置指示器圆弧宽度 */
    
    lv_obj_t * arc = lv_arc_create(lv_scr_act());
    lv_arc_set_bg_angles(arc, 0, 360);
    lv_obj_align(arc,LV_ALIGN_BOTTOM_RIGHT, 0, 0);
    
//    static lv_obj_t* main_chart;
//    main_chart = lv_obj_create(lv_scr_act());
//    lv_obj_set_size(main_chart, 400, 280);
//    lv_obj_set_pos(main_chart, 0, 20);
    
    

}

int main(void)
{
     uint16_t i;
    uint16_t lastpos[5][2]; 
    
    systick_config();   // 滴答定时器初始化
    led_gpio_config();  // led初始化
    key_gpio_config();            // key初始化
    
    nvic_priority_group_set(2);//设置中断分组为2

    
    usart_gpio_config(115200U);  // 串口0初始化
    delay_1ms(100);
    
    basic_timer_config(20-1,10000-1);  // 定时器初始化 200M/20/10,000=1000hz=1ms
    dma_config();		//配置串口DMA										// DMA配置
    
    /* 8080 mcu屏 */
    LCD_Init(); //显示屏初始化代码
    lvgl_dma_init();
    GT1151_Init();
    lv_init();
    lv_port_disp_init();
    lv_port_indev_init();
//    lv_ex_label();
    lv_demo_music();

    delay_1ms(10);
    //触摸屏
    
    while(1) 
    {
        lv_timer_handler();//更新事件
        delay_1ms(5);
    }
}
