/********************************************************************************
  * 文 件 名: main.c
  * 版 本 号: v1.0
  * 修改作者: wudonghuan
  * 修改日期: 2023年04月26日
  * 功能介绍:          
  ******************************************************************************
  * 注意事项:
*********************************************************************************/
#include "sys.h"
#include "gd32f4xx.h"

#include "systick.h"
#include <stdio.h>
#include <string.h>
#include "main.h"

#include "bsp_led.h"
#include "bsp_key.h"
#include "bsp_usart.h"
#include "bsp_dma.h"
#include "bsp_basic_timer.h"
#include "lcd.h"
#include "touch.h"
#include "exmc_sdram.h"

#include "lvgl.h"
#include "lv_port_disp.h"
#include "lv_port_indev.h"

//#include "lv_demo_benchmark.h"
#include "lv_demo_music.h"
#include "lv_demo_stress.h"
#include "lv_demo_widgets.h"// 用哪个demo就包含哪个的头文件

/************************************************
函数名称 ： main
功    能 ： 主函数
参    数 ： 无
返 回 值 ： 无
作    者 ： wudonghuan
*************************************************/
extern volatile uint8_t ltdc_finish_state;
extern volatile uint8_t tli_finish_state;
void lv_ex_label(void)
{
    lv_obj_t* switch_obj = lv_switch_create(lv_scr_act());
    lv_obj_set_size(switch_obj, 120, 60);
    lv_obj_align(switch_obj, LV_ALIGN_CENTER, 0, 0);
}

int main(void)
{
//    uint16_t ii = 0;
//    uint16_t lastpos[5][2]; 
    
    
    systick_config();   // 滴答定时器初始化
    led_gpio_config();  // led初始化
    key_gpio_config();            // key初始化
    usart_gpio_config(115200U);  // 串口0初始化
    delay_1ms(100);
    basic_timer_config(20-1,10000-1);  // 定时器初始化
//    dma_config();												// DMA配置
    
    //RGB屏需要Gram
    exmc_synchronous_dynamic_ram_init(EXMC_SDRAM_DEVICE0);
       /* RGB屏 */ 
    lv_init();
    lv_port_disp_init();
    lv_port_indev_init();
//    lv_demo_stress();
    lv_demo_benchmark();
//    lv_ex_label();
//    lv_demo_music();
//    lv_demo_widgets();			// 这里是widgets的demo函数
    printf("OK\r\n");
    LCD_ShowString(0,30,700,200,24,24,0,(uint8_t*)"480*700 RGB 2_buffer");
    LCD_ShowString(1,30,700,200,24,24,0,(uint8_t*)"480*700 RGB 2_buffer");
    
    LCD_ShowString(0,240,700,200,24,24,0,(uint8_t*)"It is LAYER0");
    LCD_ShowString(1,240,700,200,24,24,0,(uint8_t*)"It is LAYER1");
    
    LCD_ShowString(0,240,724,200,24,24,0,(uint8_t*)"QQ group:262345645");
    LCD_ShowString(1,240,724,200,24,24,0,(uint8_t*)"QQ group:262345645");    
    
    LCD_ShowString(0,240,748,200,24,24,1,(uint8_t*)"2023/4/26");
    LCD_ShowString(1,240,748,200,24,24,1,(uint8_t*)"2023/4/26");     
    while(1) 
    {
        lv_timer_handler();
        delay_1ms(5);
    }
}
