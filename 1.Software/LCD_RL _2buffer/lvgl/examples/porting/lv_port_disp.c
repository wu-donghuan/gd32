/**
 * @file lv_port_disp_templ.c
 *
 */

/*Copy this file as "lv_port_disp.c" and set this value to "1" to enable content*/
#if 1

/*********************
 *      INCLUDES
 *********************/
#include "lv_port_disp.h"
#include <stdbool.h>
#include "lcd.h"
#include "bsp_dma.h"
#include "stdio.h"
/*********************
 *      DEFINES
 *********************/
#define MY_DISP_HOR_RES 480
#define MY_DISP_VER_RES  800
//#define MY_DISP_HOR_RES 480
//#define MY_DISP_VER_RES  320
//hor_res ：水平分辨率
//ver_res ：垂直分辨率


#ifndef MY_DISP_HOR_RES
    #warning Please define or replace the macro MY_DISP_HOR_RES with the actual screen width, default value 320 is used for now.
    #define MY_DISP_HOR_RES    320
#endif

#ifndef MY_DISP_VER_RES
    #warning Please define or replace the macro MY_DISP_HOR_RES with the actual screen height, default value 240 is used for now.
    #define MY_DISP_VER_RES    240
#endif

/**********************
 *      TYPEDEFS
 **********************/
extern volatile uint8_t g_gpu_state;
extern volatile uint8_t ltdc_finish_state;
/**********************
 *  STATIC PROTOTYPES
 **********************/
static void disp_init(void);

static void disp_flush(lv_disp_drv_t * disp_drv, const lv_area_t * area, lv_color_t * color_p);

//static void gpu_fill(lv_disp_drv_t * disp_drv, lv_color_t * dest_buf, lv_coord_t dest_width,
//        const lv_area_t * fill_area, lv_color_t color);
lv_disp_drv_t disp_drv;   
/**********************
 *  STATIC VARIABLES
 **********************/

/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/
 
#define LCD_FRAME_BUF_ADDR 0XC0000000
#define GUI_TFT_BUF_START               LCD_FRAME_BUF_ADDR
#define GUI_TFT_BUF_LEN                 800 * 480 * 2

#define GUI_DISP_BUF1_START             (GUI_TFT_BUF_START + GUI_TFT_BUF_LEN + GUI_TFT_BUF_LEN)
#define GUI_DISP_BUF1_LEN               GUI_TFT_BUF_LEN
#define GUI_DISP_BUF2_START             (GUI_DISP_BUF1_START + GUI_DISP_BUF1_LEN)
#define GUI_DISP_BUF2_LEN               GUI_TFT_BUF_LEN

uint16_t buf_1[800][480] __attribute__((at(GUI_DISP_BUF1_START))); //图层1

static lv_color_t *dispbuf_1 = (lv_color_t *)GUI_DISP_BUF1_START;//直接定义数组指针，不定义数组
static lv_color_t *dispbuf_2 = (lv_color_t *)GUI_DISP_BUF2_START;


void lv_port_disp_init(void)
{
    /*-------------------------
     * Initialize your display
     * -----------------------*/
    disp_init();

    /*-----------------------------
     * Create a buffer for drawing
     *----------------------------*/

    /**
     * LVGL requires a buffer where it internally draws the widgets.
     * Later this buffer will passed to your display driver's `flush_cb` to copy its content to your display.
     * The buffer has to be greater than 1 display row
     *
     * There are 3 buffering configurations:
     * 1. Create ONE buffer:
     *      LVGL will draw the display's content here and writes it to your display
     *
     * 2. Create TWO buffer:
     *      LVGL will draw the display's content to a buffer and writes it your display.
     *      You should use DMA to write the buffer's content to the display.
     *      It will enable LVGL to draw the next part of the screen to the other buffer while
     *      the data is being sent form the first buffer. It makes rendering and flushing parallel.
     *
     * 3. Double buffering
     *      Set 2 screens sized buffers and set disp_drv.full_refresh = 1.
     *      This way LVGL will always provide the whole rendered screen in `flush_cb`
     *      and you only need to change the frame buffer's address.
     */
     
#if 0
    /* Example for 1) */
    static lv_disp_draw_buf_t draw_buf_dsc_1;
    static lv_color_t buf_1[MY_DISP_VER_RES * MY_DISP_HOR_RES];                          /*A buffer for 10 rows*/
    lv_disp_draw_buf_init(&draw_buf_dsc_1, buf_1, NULL, MY_DISP_VER_RES * MY_DISP_HOR_RES);   /*Initialize the display buffer*/
#endif

#if 0
    /* Example for 2) */
    static lv_disp_draw_buf_t draw_buf_dsc_2;
    static lv_color_t buf_2_1[MY_DISP_HOR_RES * 100];                        /*A buffer for 10 rows*/
    static lv_color_t buf_2_2[MY_DISP_HOR_RES * 100];                        /*An other buffer for 10 rows*/
    lv_disp_draw_buf_init(&draw_buf_dsc_2, buf_2_1, buf_2_2, MY_DISP_HOR_RES * 100);   /*Initialize the display buffer*/
#endif
#if 3
    /* Example for 3) also set disp_drv.full_refresh = 1 below*/
    static lv_disp_draw_buf_t draw_buf_dsc_3;



    lv_disp_draw_buf_init(&draw_buf_dsc_3, ltdc_lcd_framebuf0,ltdc_lcd_framebuf1,
                          MY_DISP_VER_RES * MY_DISP_HOR_RES);   /*Initialize the display buffer*/
#endif
    /*-----------------------------------
     * Register the display in LVGL
     *----------------------------------*/

//    static lv_disp_drv_t disp_drv;                         /*Descriptor of a display driver*/
    lv_disp_drv_init(&disp_drv);                    /*Basic initialization*/

    /*Set up the functions to access to your display*/
    
/*Create a display*/

#if 0
disp_drv.sw_rotate = 1;   // add for rotation
disp_drv.rotated = LV_DISP_ROT_270;   // add for rotation
#endif



    /*Set the resolution of the display*/
    disp_drv.hor_res = MY_DISP_HOR_RES;
    disp_drv.ver_res = MY_DISP_VER_RES;

    /*Used to copy the buffer's content to the display*/
    //屏幕刷新函数咯
    disp_drv.flush_cb = disp_flush;

    /*Set a display buffer*/
    disp_drv.draw_buf = &draw_buf_dsc_3;

//    /*Required for Example 3)*/
    disp_drv.full_refresh = 1;//全局刷新
    
    /* Fill a memory array with a color if you have GPU.
     * Note that, in lv_conf.h you can enable GPUs that has built-in support in LVGL.
     * But if you have a different GPU you can use with this callback.*/
    //disp_drv.gpu_fill_cb = gpu_fill;
    
    //USE_STDPERIPH_DRIVER,GD32F4XX,LV_CONF_INCLUDE_SIMPLE
    /*Finally register the driver*/
    lv_disp_drv_register(&disp_drv);
}

/**********************
 *   STATIC FUNCTIONS
 **********************/

/*Initialize your display and the required peripherals.*/
static void disp_init(void)
{
    /*You code here*/
     LCD_Init();
}

volatile bool disp_flush_enabled = true;

/* Enable updating the screen (the flushing process) when disp_flush() is called by LVGL
 */
void disp_enable_update(void)
{
    disp_flush_enabled = true;
}

/* Disable updating the screen (the flushing process) when disp_flush() is called by LVGL
 */
void disp_disable_update(void)
{
    disp_flush_enabled = false;
}

static void disp_flush(lv_disp_drv_t *disp_drv, const lv_area_t *area, lv_color_t *color_p)
{
    if(disp_flush_enabled) {
    uint16_t offline;
    uint32_t addr;    
        
    #if 1
    addr = (uint32_t)ltdc_framebuf[0] + 2 * (ACTIVE_WIDTH * area->y1 + area->x1); /* 输出存储器地址 */   
    #else
        
//双缓冲工程失败，由于不能同步，导致旧的缓存没来得及刷新导致闪屏
    if(ltdc_finish_state == 0)
    {
        //显示为0 写入为1
        addr = (uint32_t)ltdc_framebuf[1] + 2 * (ACTIVE_WIDTH * area->y1 + area->x1); /* 输出存储器地址 */
    }
    else
    {
        //显示为1 写入为0
        addr = (uint32_t)ltdc_framebuf[0] + 2 * (ACTIVE_WIDTH * area->y1 + area->x1); /* 输出存储器地址 */
    }
    #endif
    //设置地址
        TLI_LxFBADDR(LAYER0) &= ~(TLI_LxFBADDR_FBADD);
        TLI_LxFBADDR(LAYER0) = (uint32_t)color_p;
        tli_reload_config(TLI_FRAME_BLANK_RELOAD_EN);
    g_gpu_state = 1;
    
    }
    
    /*IMPORTANT!!!
     *Inform the graphics library that you are ready with the flushing*/
//    lv_disp_flush_ready(disp_drv);
}


/*OPTIONAL: GPU INTERFACE*/

/*If your MCU has hardware accelerator (GPU) then you can use it to fill a memory with a color*/
//static void gpu_fill(lv_disp_drv_t * disp_drv, lv_color_t * dest_buf, lv_coord_t dest_width,
//                    const lv_area_t * fill_area, lv_color_t color)
//{
//    printf("x2=%d x1=%d,y2=%d y1=%d\r\n",fill_area->x2,fill_area->x1,fill_area->y2,fill_area->y1);
//    /*It's an example code which should be done by your GPU*/
////    int32_t x, y;
////    dest_buf += dest_width * fill_area->y1; /*Go to the first line*/

////    for(y = fill_area->y1; y <= fill_area->y2; y++) {
////        for(x = fill_area->x1; x <= fill_area->x2; x++) {
////            dest_buf[x] = color;
////        }
////        dest_buf+=dest_width;    /*Go to the next line*/
////    }
//}


#else /*Enable this file at the top*/

/*This dummy typedef exists purely to silence -Wpedantic.*/
typedef int keep_pedantic_happy;
#endif
