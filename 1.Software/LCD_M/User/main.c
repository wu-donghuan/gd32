/********************************************************************************
  * 文 件 名: main.c
  * 版 本 号: v1.0
  * 修改作者: wudonghuan
  * 修改日期: 2023年04月15日
  * 功能介绍:          
  ******************************************************************************
  * 注意事项:
*********************************************************************************/
#include "sys.h"
#include "gd32f4xx.h"
#include "systick.h"
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "bsp_led.h"
#include "bsp_key.h"
#include "bsp_usart.h"
#include "bsp_dma.h"
#include "bsp_basic_timer.h"
#include "lcd.h"


/************************************************
函数名称 ： main
功    能 ： 主函数
参    数 ： 无
返 回 值 ： 无
作    者 ： wudonghuan
*************************************************/

uint16_t POINT[5]={WHITE,BLUE,RED,YELLOW,GREEN};
uint16_t BACK[5]={LIGHTGREEN,LIGHTGRAY,LGRAY,LGRAYBLUE,LBBLUE};


int main(void)
{
     uint16_t i;
    
    
    systick_config();   // 滴答定时器初始化
    led_gpio_config();  // led初始化
    key_gpio_config();            // key初始化
    usart_gpio_config(115200U);  // 串口0初始化
    delay_1ms(100);
    printf("Hello!\r\n");             // 串口打印key release!
//    basic_timer_config(20000,10000);  // 定时器初始化
//    dma_config();												// DMA配置
    /* 8080 mcu屏 */
//    LCD_Init(); //显示屏初始化代码
//    
//    LCD_Clear(BLUE);
    
    while(1) 
    {
        /* 等待数据传输完成 */
//         for(i=0;i<5;i++)
//        {
//            POINT_COLOR=POINT[i];
//            BACK_COLOR=BACK[i];
//            
//            LCD_Clear(BACK[i]);
//            LCD_ShowString(30,50,480,80,24,1,"https://lckfb.com");
//            LCD_ShowString(30,80,480,110,24,1,lcd_id);
//            delay_1ms(800);
//        }
        
        
        if(g_recv_complete_flag)  // 数据接收完成
        { 
            g_recv_complete_flag = 0;                   // 等待下次接收
            printf("g_recv_length:%d ",g_recv_length);  // 打印接收的数据长度
            printf("g_recv_buff:%s\r\n",g_recv_buff);// 打印接收的数据
            memset(g_recv_buff,0,g_recv_length);// 清空数组
            g_recv_length = 0;// 清空长度
        }
        
        
        
//        key_scan();   // 按键扫描
//        gpio_bit_write(PORT_LED1,PIN_LED1,SET);   // LED1输出高电平
//        gpio_bit_write(PORT_LED2,PIN_LED2,SET);   // LED2输出高电平
//        gpio_bit_write(PORT_LED3,PIN_LED3,SET);   // LED3输出高电平
//        gpio_bit_write(PORT_LED4,PIN_LED4,SET);   // LED4输出高电平
//        delay_1ms(100); // 延时1s
//        gpio_bit_write(PORT_LED2,PIN_LED2,RESET); // LED2输出低电平
//        gpio_bit_write(PORT_LED1,PIN_LED1,RESET);   // LED1输出高电平
//        gpio_bit_write(PORT_LED3,PIN_LED3,RESET);   // LED3输出高电平
//        gpio_bit_write(PORT_LED4,PIN_LED4,RESET);   // LED4输出高电平
//        delay_1ms(100);// 延时1s
    }
}
