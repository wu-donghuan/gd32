/********************************************************************************
  * 文 件 名: bsp_dma.c
  * 版 本 号: 初版
  * 修改作者: LC
  * 修改日期: 2022年04月21日
  * 功能介绍:          
  ******************************************************************************
  * 注意事项:
*********************************************************************************/
#include "bsp_dma.h"
#include "bsp_usart.h"

#include "lvgl.h"
#include "lv_port_disp.h"

/************************************************
函数名称 ： dma_config
功    能 ： DMA配置
参    数 ： 无
返 回 值 ： 无
作    者 ： LC
*************************************************/
void dma_config(void)
{
	dma_single_data_parameter_struct dma_init_struct;                // DMA单数据结构体
    rcu_periph_clock_enable(BSP_DMA_RCU);														// 开启DMA时钟
  	
	dma_deinit(BSP_DMA,BSP_DMA_CH);																	// 初始化DMA通道
	
	 /* 配置DMA初始化参数 */
    dma_init_struct.periph_addr 				 = (uint32_t)&USART_DATA(BSP_USART);   // 外设地址                       
    dma_init_struct.periph_inc					 = DMA_PERIPH_INCREASE_DISABLE;        // 不使用增量模式，为固定模式  
    dma_init_struct.memory0_addr 			 = (uint32_t)g_recv_buff;              // 内存地址          
    dma_init_struct.memory_inc 				 = DMA_MEMORY_INCREASE_ENABLE;      	 // 增量模式                    
    dma_init_struct.periph_memory_width = DMA_PERIPH_WIDTH_8BIT;         		 // 一次传输长度8bit         
    dma_init_struct.circular_mode 			 = DMA_CIRCULAR_MODE_DISABLE;      		 // 关闭循环模式               
    dma_init_struct.direction 					 = DMA_PERIPH_TO_MEMORY;          		 // 外设到内存                  
    dma_init_struct.number 						 = USART_RECEIVE_LENGTH;         			 // 要传输的数据量                     
    dma_init_struct.priority						 = DMA_PRIORITY_ULTRA_HIGH;  					 // 超高优先级
	 /* 初始化DMA结构体 */
	 dma_single_data_mode_init(BSP_DMA,BSP_DMA_CH,&dma_init_struct);
	
	 /* 使能通道外设 */
	 dma_channel_subperipheral_select(BSP_DMA,BSP_DMA_CH,DMA_SUBPERI4);
	 /* 使能DMA通道 */
	 dma_channel_enable(BSP_DMA,BSP_DMA_CH);
	
	 /* 使能DMA通道中断 */
	 dma_interrupt_enable(BSP_DMA,BSP_DMA_CH,DMA_CHXCTL_FTFIE);
	 /* 配置中断优先级 */
	 nvic_irq_enable(BSP_DMA_CH_IRQ, 2, 1); 
    
    /* 使能串口DMA接收 */
	 usart_dma_receive_config(BSP_USART,USART_RECEIVE_DMA_ENABLE);
     
}

/************************************************
函数名称 ： BSP_DMA_CH_IRQHandler
功    能 ： DMA中断服务函数 
参    数 ： 无
返 回 值 ： 无
作    者 ： LC
*************************************************/
void BSP_DMA_CH_IRQ_HANDLER(void)
{
	
	if(dma_interrupt_flag_get(BSP_DMA,BSP_DMA_CH,DMA_INT_FLAG_FTF) == SET)  // 传输完成中断 
	{	
	  dma_interrupt_flag_clear(BSP_DMA,BSP_DMA_CH,DMA_INT_FLAG_FTF);				// 清中断标志位
		//g_recv_complete_flag = 1;                                           // 数据传输完成 
  }
}
/************************************************
函数名称 ： lvgl_dma_init
功    能 ： lvgl_DMA配置
参    数 ： 无
返 回 值 ： 无
作    者 ： wudonghuan
*************************************************/
void lvgl_dma_init(void)
{
	dma_multi_data_parameter_struct dma_init_parameter;
	/* peripheral clock enable */
	rcu_periph_clock_enable(RCU_DMA1);
	/* DMA peripheral configure */
	dma_deinit(DMA1,DMA_CH0);
    
	dma_init_parameter.periph_width = DMA_PERIPH_WIDTH_16BIT;
	dma_init_parameter.periph_inc = DMA_PERIPH_INCREASE_ENABLE;//外设增量
    
	dma_init_parameter.memory_width = DMA_MEMORY_WIDTH_16BIT;
	dma_init_parameter.memory_inc = DMA_MEMORY_INCREASE_DISABLE;//不增量
    
	dma_init_parameter.memory_burst_width = DMA_MEMORY_BURST_8_BEAT;//存储器数据传输长度8
	dma_init_parameter.periph_burst_width = DMA_PERIPH_BURST_8_BEAT;//外设数据传输长度8
    
	dma_init_parameter.critical_value = DMA_FIFO_4_WORD;//FIF0等级 4级
    
	dma_init_parameter.circular_mode = DMA_CIRCULAR_MODE_DISABLE;//普通模式
    
	dma_init_parameter.direction = DMA_MEMORY_TO_MEMORY;//内存到内存
    
	dma_init_parameter.priority = DMA_PRIORITY_MEDIUM;//传输软件优先级

    
	dma_multi_data_mode_init(DMA1,DMA_CH0,&dma_init_parameter);
    
	dma_interrupt_disable(DMA1, DMA_CH0, DMA_CHXCTL_HTFIE);//不使能
	dma_interrupt_enable(DMA1, DMA_CH0, DMA_CHXCTL_FTFIE);//使能
	nvic_irq_enable(DMA1_Channel0_IRQn, 1, 0);
	dma_channel_disable(DMA1, DMA_CH0);
}

void lvgl_dma_transfer(uint32_t src_addr, uint32_t dst_addr, uint32_t datalength)
{
	dma_periph_address_config(DMA1, DMA_CH0, src_addr);////DMA外设地址(后续赋值)
	dma_memory_address_config(DMA1, DMA_CH0, DMA_MEMORY_0, dst_addr);//存储器设备
    
	dma_transfer_number_config(DMA1, DMA_CH0, datalength);//传输长度
    
	dma_channel_enable(DMA1, DMA_CH0);
}

/*屏幕刷新DMA*/
void DMA1_Channel0_IRQHandler(void)
{
    if(dma_interrupt_flag_get(DMA1, DMA_CH0, DMA_INT_FLAG_FTF)) 
    {
//        printf("10000");
        dma_interrupt_flag_clear(DMA1, DMA_CH0, DMA_INT_FLAG_FTF);

        lv_disp_flush_ready(&disp_drv);
    }
    
}
